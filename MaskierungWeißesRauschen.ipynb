{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Betrachten wir zuerst mal ein Sinussignal mit $f= 1\\mathrm{kHz}$ mit einer Amplitude von \n",
    "\n",
    "$ A = -42 \\mathrm{dB} = 10^{\\frac{-42}{20}} \\approx 0.08$.\n",
    "\n",
    "Die Leistung dieses Sinussignals liegt dann bei\n",
    "\n",
    "$ L = \\frac{A^2}{2} \\approx \\frac{0.008^2}{2} = 3.2 \\cdot 10^{-5} $\n",
    "\n",
    "Gehen wir davon aus, dass wir unsere Kopfhörerlautstärke so eingestellt haben, dass wir dieses Sinussignal gerade noch / nicht mehr wahrnehmen können und wir ein Normohr besitzen. Damit entspräche der Schalldruckpegel an unserem Trommelfell \n",
    "\n",
    "$ L_{\\mathrm{p}} \\approx 3.4 \\mathrm{dB}$.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook \n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "fs = 44100;\n",
    "\n",
    "A = 10**(-42/20)\n",
    "s = A* np.sin(2*np.pi*1000/fs * np.arange(0,44100,1))\n",
    "\n",
    "print('Die Amplitude des Sinussignals beträgt A = ' + str(A) )\n",
    "\n",
    "L_s = np.dot(s,s)/44100\n",
    "print('Die Leistung des Sinussignals beträgt L_s = ' + str(L_s) )\n",
    "\n",
    "p_1kHz = 10**(3.4/20)*2e-5\n",
    "print('Der Schalldruck (am Trommelfell) des Sinussignals beträgt p = ' + str(p_1kHz) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nun gilt für den Zusammenhang des Schalldrucks (nicht des Pegels) und der Schallleistung $L$:\n",
    "\n",
    "$ p = \\sqrt{\\frac{L Z}{A}} $\n",
    "\n",
    "wobei $Z$ die akustische Impedanz der Luft ist und $A$ die Fläche auf die der Schalldruck wirkt. Bei 25 Grad Celsius Raumtemperatur gilt $ Z \\approx 410$ und die Einheit schenke ich mir. Die Fläche $A$ brauchen wir für unsere Betrachtungen nicht. Entscheidend ist hier, dass \n",
    "\n",
    "$ p \\propto \\sqrt{L} $\n",
    " \n",
    "gilt. Der Schalldruck ist also proportional zur Wurzel der Schallleistung. Gehen wir mal davon aus, dass die Schallleistung ebenfalls proportional zur Signalleistung ist. Dass ist gerechtfertigt, wenn zwischen dem Array in Python und unserem Kopfhörer ein Tiefpassfilter und ein linearer Verstärker hängt und keine Nichtlinearitäten auftreten. Damit können wir also den Schalldruck berechnen, wenn wir die Signalleistung kennen:\n",
    "\n",
    "$ p = \\frac{p_{1\\mathrm{kHz}}}{\\sqrt{L_{1\\mathrm{kHz}}}} \\cdot L $\n",
    "\n",
    "Für den Schalldruckpegel gilt dann:\n",
    "\n",
    "\\begin{align}\n",
    " p_{\\mathrm{dB}} &= 20 \\log{\\frac{p}{p_0}} \\\\\n",
    "                 &= 20 \\log{\\frac{p_{1\\mathrm{kHz}}}{p_0}} + 20 \\log{\\sqrt{\\frac{L}{L_{\\mathrm{1kHz}}}}} \\\\\n",
    "                 &= 3.4 \\mathrm{dB} + 20 \\log{\\sqrt{\\frac{L}{L_{\\mathrm{1kHz}}}}}\n",
    "\\end{align}\n",
    "\n",
    "Daraus folgt, dass sich der Schalldruckpegel um $3 \\mathrm{dB}$ erhöht, wenn wir die Leistung des Signals verdoppeln bzw. die Amplitude des Signals um den Faktor $\\sqrt{2}$ erhöhen. Es ergeben sich also bei Sinussignalen mit Ampliuden $A \\in \\{-15,-18....-42\\} $ Schalldruckpegel $p \\in \\{3.4 \\mathrm{dB}, 6.4 \\mathrm{dB}, ..., 36.4 \\mathrm{dB}\\}$. Klingt irgendwie ganz schön leise. Schauen wir doch mal, ob das sinnvoll ist..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Was hat es nun mit dem $l_{\\mathrm{wn}}$ aus dem Zwicker Buch auf sich? Diese Größe ist der Pegel der Schallintensitätsdichte. Im einfachen Fall eines über die Fläche $A$ konstanten Schallfeldes gilt für die Schallintensität:\n",
    "\n",
    "$ I = \\frac{L}{A} = \\frac{p^2}{Z}$\n",
    "\n",
    "Für die Schallintensitätsdichte $ L_{\\mathrm{wn}} $ sollte dann also gelten:\n",
    "\n",
    "$ L_{\\mathrm{wn}} = \\frac{ {\\lvert S(f) \\rvert}^2 }{A} = \\frac{p_{1\\mathrm{kHz}}^2}{L_{1\\mathrm{kHz}}Z} \\cdot {\\lvert S(f) \\rvert}^2 $\n",
    "\n",
    "Im Fall von weißem Rauschen ist ${\\lvert S(f) \\rvert}^2 = N_0 = \\mathrm{const}$. Das heißt natürlich, dass weißes Rauschen eine unendliche Leistung besitzt. Das ist in der Realität nicht möglich und bei akustischen Signalen wird der Frequenzbereich meist auf $20 \\mathrm{Hz}$ bis 20 $\\mathrm{kHz}$ eingeschränkt. Somit ergibt sich für uns:\n",
    "\n",
    "${\\lvert S(f) \\rvert}^2 \\approx \\frac{L_s}{20\\mathrm{kHz}} = \\mathrm{const}$.\n",
    "\n",
    "Für den Pegel der Schallintensitätsdichte gilt dann:\n",
    "\n",
    "$ l_{\\mathrm{wn}} = 10 \\log \\frac{L_{\\mathrm{wn}}}{I_0} $ mit $ I_0 = 10^{-12} \\frac{\\mathrm{W}}{\\mathrm{m}^2}$.\n",
    "Also berechnen wir diesen Pegel doch mal für die Annahmen, die in Versuch 7 des PT-I getroffen werden:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wn = np.random.rand(882000)*2-1 #weißes Rauschen\n",
    "\n",
    "# Normierung der Rauschenergie wie in Versuch 7\n",
    "sbn = np.copy(wn)\n",
    "sbn_fft = np.fft.fft(sbn)\n",
    "sbn_fft[0:18000] = 0\n",
    "sbn_fft[22000:None] = 0\n",
    "sbn = np.fft.irfft(sbn_fft[0:int(len(sbn_fft)/2)+1])\n",
    "sbn = sbn / np.amax(np.abs(sbn))*0.5\n",
    "\n",
    "E_sbn = np.dot(sbn,sbn)\n",
    "\n",
    "E_wn = np.dot(wn,wn)\n",
    "\n",
    "wn = np.sqrt(E_sbn/E_wn) * wn\n",
    "E_wn = np.dot(wn,wn)\n",
    "\n",
    "L_wn = E_wn/882000; # Leistung des Rauschens\n",
    "\n",
    "# Berechnung des Pegels der Schallintensitätsdichte\n",
    "l_wn = 10*np.log10(p_1kHz**2 / 410 / L_s *L_wn/20000 / 1e-12)\n",
    "\n",
    "print('Der Pegel der Schallintensitätsdichte betägt l_wn = ' + str(l_wn) +' dB')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Schauen wir uns dazu nochmal die Maskierungscharakteristik von weißem Rauschen bei der Frequenz von $f = 1 \\mathrm{kHz}$ an. Wie zu erkennen ist hat Herr Zwicker keine Maskierungscharakteristik für weißes Rauschen mit unserer Schallintensitätsdichte gemessen. Lass uns also zwei Rauschsignale erzeugen, die einen Pegel der Schallintensitätsdichte von -10 dB und 0 dB aufweisen. \n",
    "\n",
    "![](Masking.png \"\")\n",
    "\n",
    "Für die Leistung von gleichverteiltem mittelwertfreien weißen Rauschen gilt:\n",
    "$ L_{\\mathrm{wn}}  = \\frac{1}{2A} 2  \\frac{A^3}{3} = \\frac{A^2}{3} $. Somit können wir die Amplitude des Rausschsignals berechnen. Für die Leistung des Rauschens gilt ebenfalls:\n",
    "$ -10 = 10 \\log \\left( \\frac{p^2_{\\mathrm{1kHz}}}{410 L_s \\cdot 200000 \\cdot 1e-12} \\right) + 10 \\log L_{\\mathrm{wn}} $. Das Einsetzen und Umformen ist jedem selber überlassen."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L_wn_minus10dB = 10**((-10 -10 * np.log10(p_1kHz**2 / 410 / L_s /20000 / 1e-12))/10)\n",
    "A_wn_minus10dB = np.sqrt(L_wn_minus10dB*3)\n",
    "\n",
    "L_wn_0dB = 10**((0 -10 * np.log10(p_1kHz**2 / 410 / L_s /20000 / 1e-12))/10)\n",
    "A_wn_0dB = np.sqrt(L_wn_0dB*3)\n",
    "\n",
    "print('Die Rauschsignalamplitude für l_wn = -10 dB beträgt: A = ' + str(A_wn_minus10dB))\n",
    "print('Die Rauschsignalamplitude für l_wn = 0 dB beträgt: A = ' + str(A_wn_0dB))\n",
    "\n",
    "wn_minus10dB = A_wn_minus10dB* (np.random.rand(882000)*2-1)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Für $l_{\\mathrm{wn}} = 0 \\mathrm{dB}$ wird es also zu clipping kommen. Insofern können wir diesen Fall mit den vorliegenden Sinussignalen kaum testen. Für diesen Fall müssten wir Sinussignalarrays anlegen, die eine niedrigere Amplitude aufweisen. Dennoch lässt sich der Test für $l_{\\mathrm{wn}} = -10 \\mathrm{dB}$ ausführen. Nach der Abbildung liegt unsere Ruhehörschwelle bei diesem Maskierer und der Frequenz $ f = 1 \\mathrm{kHz}$ bei ca. $ L_{\\mathrm{p}} = 10 \\mathrm{dB}$. Wir sollten also in der Lage sein, alle Sinussignale mit den Schalldruckpegeln $ p > 10 \\mathrm{dB} $ wahrzunehmen. Daraus folgt, dass wir ca 7 Sinussignaltöne wahrnehmen sollten. Das klingt für mich schon ohne es getestet zu haben viel zu plausibel ;-)...."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = np.arange(-15,-43,-3)\n",
    "sig = np.array([])\n",
    "for ii in A:\n",
    "    s = 10**(ii/20)*np.sin(1000/44100*2*np.pi*np.arange(0,44100,1))\n",
    "    p = np.zeros(44100)\n",
    "    sig = np.hstack((sig,s,p))    \n",
    "    \n",
    "ramp = np.hstack((np.arange(0,1,1/2000),np.ones(40100),np.arange(1,0,-1/2000)))\n",
    "ramp_seq = np.tile(ramp,20)\n",
    "\n",
    "sin_seq = sig*ramp_seq\n",
    "\n",
    "sin_seq_wn_minus10dB = wn_minus10dB + sin_seq"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import IPython.display as ipd\n",
    "def play(s, fs, autoplay=False):\n",
    "    display(ipd.Audio(s, rate=fs, autoplay=autoplay))\n",
    "\n",
    "play(sin_seq,44100)\n",
    "play(sin_seq_wn_minus10dB,44100)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Für mein Ohr haut das ganz gut hin. Schauen wir doch mal, ob das Zufall war...\n",
    "Laut Abbildung sollten bei $ l_{\\mathrm{wn}} = 0  \\mathrm{dB} $ noch ca. 4 Sinussignale wahrnehmbar sein. Dafür müssen wir aber, wie oben erwähnt, den Aufbau neu kalibrieren. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = 10**(-51/20)\n",
    "s = A* np.sin(2*np.pi*1000/fs * np.arange(0,44100,1))\n",
    "L_s = np.dot(s,s)/44100\n",
    "print('Die Leistung des Sinussignals beträgt ' + str(L_s) )\n",
    "\n",
    "L_wn_0dB = 10**((0 -10 * np.log10(p_1kHz**2 / 410 / L_s /20000 / 1e-12))/10)\n",
    "A_wn_0dB = np.sqrt(L_wn_0dB*3)\n",
    "\n",
    "print('Die Rauschsignalamplitude für l_wn = 0 dB beträgt: A = ' + str(A_wn_0dB))\n",
    "\n",
    "A = np.arange(-24,-52,-3)\n",
    "sig = np.array([])\n",
    "for ii in A:\n",
    "    s = 10**(ii/20)*np.sin(1000/44100*2*np.pi*np.arange(0,44100,1))\n",
    "    p = np.zeros(44100)\n",
    "    sig = np.hstack((sig,s,p))    \n",
    "    \n",
    "ramp = np.hstack((np.arange(0,1,1/2000),np.ones(40100),np.arange(1,0,-1/2000)))\n",
    "ramp_seq = np.tile(ramp,20)\n",
    "\n",
    "sin_seq = sig*ramp_seq\n",
    "\n",
    "\n",
    "wn_0dB = A_wn_0dB* (np.random.rand(882000)*2-1)\n",
    "\n",
    "sin_seq_wn_0dB = wn_0dB + sin_seq\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import IPython.display as ipd\n",
    "def play(s, fs, autoplay=False):\n",
    "    display(ipd.Audio(s, rate=fs, autoplay=autoplay))\n",
    "\n",
    "play(sin_seq,44100)\n",
    "play(sin_seq_wn_0dB,44100)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Auch das funktioniert ganz akzeptabel. Bei $ l_{\\mathrm{wn}} = 10 \\mathrm{dB} $ sollte man das Sinussignal noch ca. 2-3 mal hören können. Schauen wir mal. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = 10**(-60/20)\n",
    "s = A* np.sin(2*np.pi*1000/fs * np.arange(0,44100,1))\n",
    "L_s = np.dot(s,s)/44100\n",
    "print('Die Leistung des Sinussignals beträgt ' + str(L_s) )\n",
    "\n",
    "L_wn_10dB = 10**((10 -10 * np.log10(p_1kHz**2 / 410 / L_s /20000 / 1e-12))/10)\n",
    "A_wn_10dB = np.sqrt(L_wn_10dB*3)\n",
    "\n",
    "print('Die Rauschsignalamplitude für l_wn = 10 dB beträgt: A = ' + str(A_wn_10dB))\n",
    "\n",
    "A = np.arange(-33,-61,-3)\n",
    "sig = np.array([])\n",
    "for ii in A:\n",
    "    s = 10**(ii/20)*np.sin(1000/44100*2*np.pi*np.arange(0,44100,1))\n",
    "    p = np.zeros(44100)\n",
    "    sig = np.hstack((sig,s,p))    \n",
    "    \n",
    "\n",
    "ramp = np.hstack((np.arange(0,1,1/2000),np.ones(40100),np.arange(1,0,-1/2000)))\n",
    "ramp_seq = np.tile(ramp,20)\n",
    "\n",
    "sin_seq = sig*ramp_seq\n",
    "\n",
    "\n",
    "wn_10dB = A_wn_10dB * (np.random.rand(882000)*2-1)\n",
    "\n",
    "sin_seq_wn_10dB = wn_10dB + sin_seq\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import IPython.display as ipd\n",
    "def play(s, fs, autoplay=False):\n",
    "    display(ipd.Audio(s, rate=fs, autoplay=autoplay))\n",
    "\n",
    "play(sin_seq,44100)\n",
    "play(sin_seq_wn_10dB,44100)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ich höre da kein Sinussignal mehr. Andere meinen, sie hätten 2 gehört...\n",
    "\n",
    "*Wenn man aus dem Wasser kommt ist man immer nass*\n",
    "*-Ok,ich hab' verstanden.*\n",
    "*Hammer, fett, bombe, krass*"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
